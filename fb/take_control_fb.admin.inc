<?php


include_once (drupal_get_path('module', 'take_control') . '/includes/security.inc');
include_once (drupal_get_path('module', 'take_control') . '/includes/filesystem.inc');

/**
 * take control admin configuration form.
 */
function take_control_fb_settings_form(&$form_state = array()) {
  $extvers = take_control_extjs_dirs();

  $form['take_control_fb_extjs_ver'] = array(
      '#type' => 'select',
      '#options' => drupal_map_assoc($extvers),
      '#default_value' => variable_get('take_control_fb_extjs_ver', ''),
      '#required' => TRUE,
      '#title' => t('ExtJs version directory'),
      '#description' => t('ExtJs version directory (in /sites/all/modules) that should be used by the File Browser module.'));

  $form['#submit'][] = 'take_control_fb_settings_submit_handler';

  return system_settings_form($form);
}

/**
 * Custom submit handler to process custom form data.
 */
function take_control_fb_settings_submit_handler($form, &$form_state) {
  variable_set('take_control_fb_extjs_ver', $form_state['values']['take_control_fb_extjs_ver']);
}
